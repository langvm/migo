// Copyright 2023-2024 LangVM Project
// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0
// that can be found in the LICENSE file and https://mozilla.org/MPL/2.0/.

package migo

import (
	ast "migo/syntax"
)

type Type struct {
}

type FileVisitor struct {
	GlobalFuncSet map[string]*ast.FuncDecl
}

func (c *FileVisitor) Collect(file *ast.File) {
	for _, decl := range file.DeclList {
		switch decl := decl.(type) {
		case *ast.FuncDecl:
			results := decl.Type.ResultList
			if len(results) != 0 {
				if results[len(results)-1].Name.Value == "error" {
					c.GlobalFuncSet[decl.Name.Value] = decl
				}
			}
		}
	}
}

func (c *FileVisitor) Visit(node ast.Node) ast.Visitor {
	switch n := node.(type) {
	case *ast.AssignStmt:
		list := n.Lhs.(*ast.ListExpr).ElemList
		if len(list) != 0 {
		}
	case *ast.FuncDecl:
	}

	return c
}

type FuncVisitor struct {
	Fn      *ast.FuncDecl
	FuncSet map[string]*ast.FuncType
}

func (v *FuncVisitor) GenerateReturnStmtWithError(expr ast.Expr) ast.Expr {
	for _, result := range v.Fn.Type.ResultList {
		switch result.Type.GetTypeInfo() {
		}
	}
}

func (v *FuncVisitor) WalkScope(blockStmt []ast.Stmt) {
	var stmts []ast.Stmt
	for _, stmt := range blockStmt {
		switch stmt := stmt.(type) {
		case *ast.IfStmt:
			v.WalkScope(stmt.Then.List)
		case *ast.ForStmt:
			v.WalkScope(stmt.Body.List)
		case *ast.SwitchStmt:
			for _, clause := range stmt.Body {
				v.WalkScope(clause.Body)
			}
		case *ast.SelectStmt:
			for _, clause := range stmt.Body {
				v.WalkScope(clause.Body)
			}
		case *ast.LabeledStmt:
			v.WalkScope([]ast.Stmt{stmt.Stmt}) // TODO: Check
		}
		switch stmt := stmt.(type) {
		case *ast.CallStmt:
			if stmt.Tok.String() == "panic" {
				stmts = append(stmts, &ast.ReturnStmt{
					Results: &ast.ListExpr{
						ElemList: []ast.Expr{
							v.GenerateReturnStmtWithError(stmt.Call.(*ast.CallExpr).ArgList[0])}}})
			}
		default:
			stmts = append(stmts, stmt)
		}
	}
}
